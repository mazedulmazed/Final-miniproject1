-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2016 at 03:24 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miniphonebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonebook`
--

CREATE TABLE `phonebook` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `group` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `added_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phonebook`
--

INSERT INTO `phonebook` (`id`, `name`, `mobile`, `group`, `profile_picture`, `added_by`) VALUES
(54, 'Bill Gates', '666777', 'Friends', '/SecurePhonebook/resource/uploads/default.jpg', 'hasin@webmail.com'),
(55, 'Homo Sapiece', '989898', 'Work', '/SecurePhonebook/resource/uploads/uuu.jpg', 'towfiqpiash@gmail.com'),
(74, 'Md. Mazedul  Islam', '01835532112', 'Work', '/SecurePhonebook/resource/uploads/mazed.jpg', 'mazed_30@yahoo.com'),
(75, 'Sanowar Hossain', '01975532112', 'Family', '/SecurePhonebook/resource/uploads/Penguins.jpg', 'mazed_30@yahoo.com'),
(76, 'Arman  Nasir', '01975532112', 'Work', '/SecurePhonebook/resource/uploads/Koala.jpg', 'mazed_30@yahoo.com'),
(77, 'Bill Grates', '95071124', 'Work', '/SecurePhonebook/resource/uploads/bil grates.jpg', 'mazed_30@yahoo.com'),
(78, 'Azim  Premji', '01835532112', 'Friends', '/SecurePhonebook/resource/uploads/45-ray-dalio.jpg', 'mazed_30@yahoo.com'),
(79, 'Charlie  Ergen', '95071125', 'Work', '/SecurePhonebook/resource/uploads/46-charlie-ergen.jpg.png', 'mazed_30@yahoo.com'),
(80, 'TIE: Lei  Jun', '01975808006', 'Work', '/SecurePhonebook/resource/uploads/47-tie-lei-jun.jpg', 'mazed_30@yahoo.com'),
(81, ' Donald  Bren', '85968596', 'Other', '/SecurePhonebook/resource/uploads/41-donald-bren.jpg', 'mazed_30@yahoo.com'),
(82, 'Bill Gates', '01975532112', 'Family', '/SecurePhonebook/resource/uploads/bil grates.jpg', 'mazed_300@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`) VALUES
(17, 'mazed_30@yahoo.com', '0dab29e98e9d7433ac54e90f74e55534'),
(18, 'mazed_300@yahoo.com', '0dab29e98e9d7433ac54e90f74e55534');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phonebook`
--
ALTER TABLE `phonebook`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phonebook`
--
ALTER TABLE `phonebook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
