<?php
    include_once '../startup.php';
    use App\User\Auth;
    use App\Utility\Direction;
    use App\Contact\Phonebook;
    
    $objAuth = new Auth();
    $status = $objAuth->is_loggedin();

    if($status == false){
        return Direction::redirect("../../index.php"); 
    } else{
        $objContact = new Phonebook();
        $person = $objContact->show($_GET['id']);
    }    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Single Contact</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php require_once('../Layout/common_style.php'); ?>
    </head>
    <body>
        <?php require_once('../Layout/navbar.php'); ?>

        <div class="container">
            <h1 class="text-center"><span class="glyphicon glyphicon-user"></span> <?= $person->name; ?></h1>
            <hr/>
            <div class="jumbotron col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-3">
                        <p class="img-container img-responsive">
                            <img src="<?= $person->profile_picture; ?>" class="img-thumbnail" alt="user">
                        </p>
                    </div>
                    <div class="col-md-9">
                        <p>
                            <label>Name: </label>
                            <span><?= $person->name; ?></span>
                        </p>
                        <p>
                            <label>Mobile: </label>
                            <span><?= $person->mobile; ?></span>
                        </p>
                        <p>
                            <label>Group: </label>
                            <span><?= $person->group; ?></span>
                        </p>
                    </div>
                </div>

                <div class="form-group">
                    <a href="javascript:history.go(-1)" class="btn btn-primary"><span class="glyphicon glyphicon-menu-left"></span> Back</a>
                    <a href="edit.php?id=<?= $person->id; ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    <form class="form-inline" action="delete.php" method="POST" style="display: inline;">
                        <input type="hidden" name="id" value="<?= $person->id; ?>"/>
                        <button class="btn btn-danger deleteBtn" type="submit"><span class="glyphicon glyphicon-remove"></span> Delete</button>
                    </form>
                </div>
            </div>
        </div>

        <?php require_once('../Layout/footer.php'); ?>
        <?php require_once('../Layout/common_script.php'); ?>
        
        <script>
            $('.deleteBtn').bind('click', function(e){
                var deleteItem = confirm("Do you want to delete this? ");
                if(!deleteItem){
                    e.preventDefault();
                }
            });
        </script>
    </body>
</html>