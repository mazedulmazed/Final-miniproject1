<?php
    include_once '../startup.php';
    use App\User\Auth;
    use App\Utility\Direction;
    use App\Contact\Phonebook;
    
    $objAuth = new Auth();
    $status = $objAuth->is_loggedin();

    if($status == false){
        return Direction::redirect("../../index.php"); 
    } else{
        $objContact = new Phonebook();
        $person = $objContact->show($_GET['id']);
        $name = explode(" ", $person->name);
    }    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Edit/Update Contact</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php require_once('../Layout/common_style.php'); ?>
        <link rel="stylesheet" href="../../resource/css/fileinput.min.css">
    </head>

    <body>

        <?php require_once('../Layout/navbar.php'); ?>

        <div class="container">
            <h1 class="text-center">Edit/Update Contact</h1>
            <hr/>
            <div class="jumbotron col-md-8 col-md-offset-2">
                <form role="form" action="update.php" method="POST" enctype="multipart/form-data">
                    <h3>Personal</h3>
                    <input type="hidden" name="id" value="<?= $person->id; ?>">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="first-name">First Name: </label><span style="color: red;"> &starf;</span>
                                <input type="text" name="name[]" class="form-control" id="first-name" autofocus="autofocus" required="required" value="<?= $name['0']?>">
                            </div>
                            <div class="col-md-6">
                                <label for="last-name">Last Name:</label><span style="color: red;"> &starf;</span>
                                <input type="text" name="name[]" class="form-control" id="last-name" required="required" value="<?= $name['1']?>">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="profile_picture">Choose Profile Picture:</label>
                        <input type="file" name="profile_picture" class="file" id="profile_picture">
                        <small><i>(file size mustn't exceed 200kb. jpg/png allowed)</i></small>
                    </div>
                   
                    <h3>Contact</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="mobile">Mobile: </label><span style="color: red;"> &starf;</span>
                                <input type="tel" name="mobile" class="form-control" id="mobile" required="required" value="<?= $person->mobile; ?>">
                            </div>
                            <div class="col-md-6">
                                <label for="group">Contact Group:</label>
                                <select class="form-control" name="group" id="group">
                                    <option value="Friends" <?= ($person->group == 'Friends')?'selected="selected"':''; ?>>Friends</option>
                                    <option value="Family" <?= ($person->group == 'Family')?'selected="selected"':''; ?>>Family</option>
                                    <option value="Work" <?= ($person->group == 'Work')?'selected="selected"':''; ?>>Work</option>
                                    <option value="Other" <?= ($person->group == 'Other')?'selected="selected"':''; ?>>Other</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group pull-right">
                        <a href="javascript:history.go(-1)" class="btn btn-primary"><span class="glyphicon glyphicon-menu-left"></span> Back</a>
                        <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                    </div>
                </form>
            </div>
        </div>

        <?php require_once('../Layout/footer.php'); ?>
        <?php require_once('../Layout/common_script.php'); ?>
        <script src="../../resource/js/fileinput.min.js"></script>

        <script type="text/javascript">
            $("#profile_picture").fileinput({
                'initialPreview': [
                    '<img src="<?= $person->profile_picture; ?>" class="file-preview-image" alt="Profile Picture" title="Profile Picture">'
                ],
                'overwriteInitial': true,
                'allowedFileExtensions' : ['jpg', 'png'],
                'maxFileSize': 200
            });
            
            $('.alert').fadeOut(4000);
        </script>
    </body>
</html>