<?php

include_once '../startup.php';

use App\User\Auth;
use App\Utility\Direction;

$objAuth = new Auth();
$loggedout = $objAuth->logout();

if($loggedout){
    return Direction::redirect("../../index.php"); 
 }