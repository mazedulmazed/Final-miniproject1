<?php

include_once '../startup.php';

use App\User\Auth;
use App\Utility\Message;
use App\Utility\Direction;

$objAuth = new Auth();
$registered = $objAuth->is_registered($_POST);

if($registered){
    $_SESSION['userid'] = $_POST['email'];
    Message::message("<div class='alert alert-success'>Welcome to <b>SecurePhonebook !</b></div>");
    return Direction::redirect("../phonebook/index.php"); 
} else{
    Message::message("<div class='alert alert-danger'>Wrong user credentials! Please enter correct email/password</div>");
    return Direction::redirect("../phonebook/index.php"); 
}