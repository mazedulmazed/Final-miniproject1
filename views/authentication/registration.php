<?php

include_once '../startup.php';

use App\User\Auth;
use App\User\User;
use App\Utility\Message;
use App\Utility\Direction;

$objAuth = new Auth();
$exist = $objAuth->is_exist($_POST);

if($exist){
    Message::message("<div class='alert alert-danger'>This email is already registered! Try another email address</div>");
    Direction::redirect("../../index.php");
} else{
    $objUser = new User();
    $objUser->store($_POST);
}